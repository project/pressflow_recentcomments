<?php

/**
 * Implementation of hook_menu(),
 */
function pressflow_recentcomments_menu($may_cache) {
  $items = array();
  
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/pressflow_recentcomments',
      'title' => t('PressFlow Recent Comments'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('pressflow_recentcomments_settings'),
      'type' => MENU_NORMAL_ITEM,
      'access' => user_access('administer pressflow_recentcomments'),
    );
  }
  
  return $items;
}


/**
 * Implementation of hook_perm().
 */
function pressflow_recentcomments_perm() {
  return array('administer pressflow_recentcomments');
}


/**
 * Implementation of hook_block().
 */
function pressflow_recentcomments_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    $block[0]['info'] = t('PressFlow: Recent comments');
    return $block;
  }
  else if ($op == 'view') {
    $block['subject'] = variable_get('pressflow_recentcomments_title', 'Recent comments');
    $block['content'] = _pressflow_recentcomments_getcontent(variable_get('pressflow_recentcomments_num', 10));
    return $block;
  }
}


/**
 * Implementation of hook_settings().
 */
function pressflow_recentcomments_settings() {
  // Only administrators can access this module
  if (!user_access('administer pressflow_recentcomments')) {
    drupal_access_denied();
  }

  $form['pressflow_recentcomments_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title of the block'),
    '#default_value' => variable_get('pressflow_recentcomments_title', 'Recent comments'),
    );

  $form['pressflow_recentcomments_num'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of comments to display'),
    '#default_value' => variable_get('pressflow_recentcomments_num', 10),
    '#maxlength' => '3',
    '#size' => '3',
    );

  $form['pressflow_recentcomments_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display comments for only these content types'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('pressflow_recentcomments_nodetypes', array('story')),
  );

  return system_settings_form($form);
}


/**
 * Get the themed comment list.
 *
 * @param $num int
 *   The number of comments to retrieve.
 *
 * @return string
 *   The rendered HTML links.
 */
function _pressflow_recentcomments_getcontent($num) {
  $node_types = variable_get('pressflow_recentcomments_nodetypes', array('story'));
  $types      = "'" . implode("', '", $node_types) . "'";

  $query = 'SELECT subject, name, c.timestamp, cid, c.nid
                   FROM {comments} c INNER JOIN {node} n ON (n.nid = c.nid)
            WHERE c.status = 0 AND n.type IN (' . $types . ')
            ORDER BY timestamp DESC';
  $query = db_rewrite_sql($query, '{comments}', 'cid');
  $result = db_query_range($query, 0, $num);

  while ($comment = db_fetch_object($result)) {
    $item = theme('pressflow_recentcomments_link', $comment);
    $items[] = $item;
  }

  return theme('item_list', $items);
}


/**
 * Return a link to the comment.
 *
 * @param $comment
 *   The comment object from _pressflow_recentcomments_getcontent().
 */
function theme_pressflow_recentcomments_link($comment) {
  $link_text = '';

  if (!empty($comment->name)) {
    $link_text .= $comment->name .': ';
  }

  // Theming example: Use the node's title in the comment link
  /*
  $node = node_load($comment->nid);
  $link_text .= $node->title . ': ';
  */

  $link_text .= $comment->subject;

  return l($link_text, 'node/' . $comment->nid, array(), NULL, 'comment-' . $comment->cid);
}
